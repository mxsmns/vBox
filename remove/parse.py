import sys
import time
import Queue
import datetime
import pexpect

Testing = True

Debug = False

obd_address = 'B4:99:4C:67:91:8C'
#obd_address = '54:74:F9:9C:E4:D2'
# Add function to query if there is a connection. If so, continue
# If not, reset the connection and connect again

def createConnection(address, log):
	"""Create a pexpect connection"""
	print("Creating connection")
	connection = pexpect.spawn('gatttool -i -hci0 -b ' + address + ' -I')
	fLog = open(log, 'wb')
	connection.logfile = fLog
	return connection

def getCharacteristics(connection):
	connection.sendline('characteristics')
	connection.expect('\[LE\]')

def connect(address, log):
    """Connect to the given address and write output to log"""
    connection = createConnection(address, log)
    print "Started gatttool..."
    print("Connecting to " + address + "...")
    connection.expect('\[LE\]')
    connection.sendline('connect')
    i = connection.expect('\[LE\]>')
    #print i
		# if i == 1:
		# 	print("Device not found")
		# 	connection.kill(0)
		# if i == 0:
		# 	print("Connected")
		# 	return connection
	#except:
    return connection
	#	if Debug:
	#		print("Exception was thrown")
	#		print("debug information:")
	#		print(str(connection))

def createValue(rawData):
	return 1

# if connection == None:
# 	print "No connection"
# 	sys.exit()

# Wait for notifications
	if connection != None:
		while True:
			connection.expect('value: ')
			rval = connection.before
			print("Value: " + str(rval))

types = [
         'TIMESTAMP', 
         'SPEED', 
         'RPM', 
         'OTHER'
         ]

pids = {
        '10f0' : 'SPEED',
        '1101' : 'RPM'
        }

q = Queue.Queue()

class OBDData:
    """ Structure to hold OBD data """
    def __init__(self, value):
        # Time: 0-3rd bytes (Ignore)
        # PID: 4th-5th bytes
        # Flags: 6th byte (Ignore)
        # Checksum: 7th byte
        # Values: 8th-11th, 12th-15th, 16th-19th
        tempPid = value[4] + value[5]
        if tempPid in pids:
            self.pid = pids[tempPid]
        else:
            self.pid = "UNKNOWN"
        self.checksum = value[7]
        self.values = [''.join(value[8:12]), ''.join(value[13:17]), ''.join(value[18:22])]
        #self.time = strftime("%Y-%m-%d-%H:%M:%S:%f")
        self.time = datetime.datetime.utcnow().strftime('%Y-%m-%d-%H:%M:%S.%f')
        
    def write(self, file):
        """ Write OBD data to a file """
        file.write(self.time),
        file.write("\n")

if __name__ == "__main__":
    logfile = open('test.log', 'w')
    
    connection = connect(obd_address, 'test.log')
    print('Waiting for data...')
    while True:
        try:
            connection.expect('value: ', timeout=10)
            temp = connection.before
            temp = temp.split('Notification')
            value = temp[0]
            print("Value: " + str(value) + '\n\n')
            #print 'Value received'
        except:
            print "Didn't receive any data"
            break
    logfile.write("\n")
    logfile.close()
    #inFile.close()
