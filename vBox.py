from Camera import *
from threading import Thread
from Queue import Queue, Empty
from time import sleep
from Logger import *
from OBD import *
import os
from gps import *
import subprocess
import pexpect

Inside = False
LogBluetooth = True
Testing = False

obd_address = 'B4:99:4C:67:91:8C'

LogDirectory = "/media/card/logs/"
ImageDirectory = "/media/card/images/"

# Notify camera thread to take picture
cameraQ = Queue()

# Contains values from the OBD adapter waiting to be parsed
valueQ = Queue()

# Notify logger thread to record data
logQ = Queue(1)

# Contains the most up-to-date values to be logged
currentValues = CurrentOBD()

# Contains the name of the log directory
logNameQ = Queue(1)

# Values used to determine events
prevSpeed = 0
prevTime = time.time()

# Most recent GPS values
gpsd = None

class CameraThread(Thread):
    def __init__(self, camera):
        Thread.__init__(self)
        self.camera = camera
        self.running = True
        self.started = False

    def run(self):
        print "Camera thread started\n"
        self.started = True
        logName = logNameQ.get(block=True, timeout=10)
        if not os.path.exists("/media/card/images/" + logName):
            os.makedirs("/media/card/images/" + logName)

        while self.running:
            name = get_timestamp()
            takePicture(self.camera, ImageDirectory + logName + name)
            print "Captured image at time " + name
            sleep(3)
            # try:
            #     ready = cameraQ.get(block=True, timeout=30)
            #     name = get_timestamp()
            #     if ready == "kill":
            #         print "Recieved kill signal"
            #         break
            #     takePicture(self.camera, ImageDirectory + logName + name)
            #     print "Captured image at time " + name
            #     cameraQ.task_done()
            # except Empty:
            #     print "Timed out waiting for new image"
            #     killThreads()
            #     break

def createConnection(address, log):
    """ Create a pexpect connection """
    print("Creating bluetooth connection")
    connection = pexpect.spawn('gatttool -i -hci0 -b ' + address + ' -I')
    
    # if LogBluetooth:
        # btLog = open("raw.log", 'wb')
        #connection.logfile = btLog

def connect(address, log):
    """ Connect to the given addres and write output to log"""
    #connection = createConnection(address, log)
    connection = pexpect.spawn('gatttool -i -hci0 -b ' + address + ' -I')
    print "Started gatttool..."
    print("Connecting to " + address + "...")
    connection.expect('\[LE\]')
    connection.sendline('connect')
    i = connection.expect('\[LE\]>')
    return connection

class BluetoothThread(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.running = True
        self.started = False

    def run(self):
        print "Bluetooth thread started\n"
        self.started = True
        connection = connect(obd_address, 'test.log')
        #print connection
        while self.running:
            try:
                connection.expect('value: ')
                temp = connection.before
                temp = temp.split('Notification')
                value = temp[0]
                value = value[:-3]
                value = ''.join(value)
                # print value                
                valueQ.put(value)
            except:
                print "Didn't receive any data"
                break

def getValue(value):
    """ Strips everything but the hex bytes from the input """
    temp = value.partition("value:")
    value = temp[2]
    return value.split()

class FileThread(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.running = True
        self.started = False

    def run(self):
        print "File test started\n"
        inFile = open('long.txt', 'r')
        for i in range(0, 5):
            inFile.readline()
        for i in range (0, 5000):
            temp = inFile.readline()
            valueQ.put(temp)
            sleep(0.01)

def kiloToMile(kmh):
    return 0.621371 * kmh

class ParseThread(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.running = True
        self.started = False

    def run(self):
        print "Parsing thread started\n"
        self.started = True
    
        while self.running:
            try: 
                value = valueQ.get(block=True, timeout=10)
                if value == "kill":
                    print "Received kill signal"
                    break

                # Removes the garbage characters at the end that we don't want
                temp = value.split()[:-1]
                length = len(temp)
                value = ' '.join(temp)

                if length >= 12:
                    pidName = lookupPID(value)
                    checksum = getChecksum(value)

                    if pidName != 'UNKNOWN' and checksum == 0:
                        pid = PID(value, pidName)
                        obd = OBDValue(value, pid)
                        # speed
                        if pidName == "PID_SPEED":
                            speed = kiloToMile(obd.value)
                            if speed >= 0 and speed < 200:
                                if speed < 1.0:
                                    speed = 0.0
                                currentValues.update(OBDData.speed, str(speed))
                                #print "New speed: " + str(speed)

                        # fuel
                        if pidName == "PID_FUEL_LEVEL":
                            fuel = obd.value
                            if fuel >= 1 and fuel <= 100:
                                currentValues.update(OBDData.fuel, str(fuel))
                                #print "New fuel: " + str(fuel) 

                        # coolant temp
                        if pidName == "PID_COOLANT_TEMP":
                            coolantTemp = obd.value
                            if coolantTemp >= 0 and coolantTemp < 1000:
                                currentValues.update(OBDData.coolantTemp, str(coolantTemp))
                                #print "New coolant temp: " + str(coolantTemp)   

                        # engine load
                        if pidName == "PID_ENGINE_LOAD":
                            engineLoad = obd.value
                            if engineLoad >= 1 and engineLoad <= 100:
                                currentValues.update(OBDData.engineLoad, str(engineLoad))
                                #print "New engine load: " + str(engineLoad) 

                        # rpm
                        if pidName == "PID_RPM":
                            rpm = obd.value
                            if rpm >= 10.0 and rpm < 12000.0:
                                currentValues.update(OBDData.rpm, str(rpm))
                                #print "New rpm: " + str(rpm)

                        # throttle
                        if pidName == "PID_THROTTLE":
                            throttle = obd.value
                            if throttle >= 1 and throttle <= 100:
                                currentValues.update(OBDData.throttle, str(throttle))
                                #print "New throttle: " + str(throttle)    

                        # distance
                        if pidName == "PID_DISTANCE":
                            distance = obd.value
                            if distance >= 1:
                                currentValues.update(OBDData.distance, str(distance))
                                #print "New distance: " + str(distance)    

                        # barometric
                        if pidName == "PID_BAROMETRIC":
                            barometric = obd.value
                            if barometric >= 0 and barometric < 1000:
                                currentValues.update(OBDData.barometric, str(barometric))
                                #print "New barometric: " + str(barometric)       

                        # ambient temp
                        if pidName == "PID_AMBIENT_TEMP":
                            ambientTemp = obd.value
                            if ambientTemp >= -100 and ambientTemp < 200:
                                currentValues.update(OBDData.ambientTemp, str(ambientTemp))
                                #print "New ambientTemp: " + str(ambientTemp)        

                        # intake temp
                        if pidName == "PID_INTAKE_TEMP":
                            intakeTemp = obd.value
                            if intakeTemp >= -100 and intakeTemp < 200:
                                currentValues.update(OBDData.intakeTemp, str(intakeTemp))
                                #print "New intakeTemp: " + str(intakeTemp) 

            except Empty:
                print "No value found in queue"
                break

class GPSThread(Thread):
    def __init__(self):
        Thread.__init__(self)
        global gpsd
        gpsd = gps(mode=WATCH_ENABLE)
        self.running = True
        self.started = False

    def run(self):
        print "GPS thread started\n"
        self.started = True
        while self.running:
            # Only waits when data is available
            if gpsd.waiting():
                gpsd.next()


class LoggerThread(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.running = True
        self.started = False

    def run(self):
        print "Logging thread started\n"
        self.started = True
        # Check to see which logs already exist
        i = 0
        while True:
            if os.path.isfile(LogDirectory + "log" + str(i) + ".log"):
                i = i + 1
            else:
                break

        logName = "log" + str(i)
        logNameQ.put(logName + "/")
        logName = "/media/card/logs/" + logName

        flogger = FileLogger(logName + ".log",
                                [['Speed', 'MPH'],
                                ['RPM', 'RPM'], 
                                ['Throttle', '%'], 
                                ['EngineLoad', '%'], 
                                ['Fuel', '%'], 
                                ['Barometric', 'kPa'], 
                                ['AmbientTemperature', 'C'], 
                                ['CoolantTemperature', 'C'], 
                                ['IntakeTemperature', 'C'], 
                                ['Distance', 'km'], 
                                ['Events']])

        cameraTimer = 0
        global cameraQ
        global currentValues

        latitude = str(gpsd.fix.latitude)
        longitude = str(gpsd.fix.longitude)
        speed = str(gpsd.fix.speed)

        # Delay between logs (in seconds)
        delay = 1

        # Delay between image captures (in seconds)
        camDelay = 5
        
        prevTime = time.time()
        prevSpeed = 0
        event = 0
        while self.running:
            timestamp = get_timestamp()

            # Check if event
            """
            taken = time.time() - prevTime
            if not currentValues.speed == "XX":
                speedDiff = float(currentValues.speed) - prevSpeed
                prevSpeed = float(currentValues.speed)
                acceleration = speedDiff #/ float(taken)
            else:
                acceleration = 0
            # acceleration = (currentValues.speed - prevSpeed) / (timestamp - prevTime)
            #print acceleration
            print "Acceleration %s" % acceleration
            if acceleration > 2:
                event = 1
            elif acceleration < -2:
                event = 2
            else:
                event = 0
            """
            # If event take picture
            if (event > 0):
                cameraQ.put(timestamp)
                cameraTimer = 0

            # else if picture timer hits 3 take picture
            elif cameraTimer == 3:# (camDelay / delay) - 1:
                cameraQ.put(timestamp)
            # Log the current data

            # if not latitude == str(gpsd.fix.latitude):
            #print "Location changed"
            #print currentValues.speed

            if not latitude == str(gpsd.fix.latitude):
                print "Location changed"
 
                latitude = str(gpsd.fix.latitude)
                longitude = str(gpsd.fix.longitude)
                # speed = str(gpsd.fix.speed)
                if not currentValues.speed == "XX":
                    acceleration = float(currentValues.speed) - prevSpeed
                else:
                    acceleration = 0
                prevSpeed = float(currentValues.speed)
              	print "Acceleration %s" % acceleration 
                if acceleration > 4:
                    event = 1
                elif acceleration < -2:
                    event = 2
                else:
                    event = 0

                flogger.log([timestamp, 
                                str(latitude), 
                                str(longitude), 
                                currentValues.speed, 
                                currentValues.rpm, 
                                currentValues.throttle, 
                                currentValues.engineLoad, 
                                currentValues.fuel, 
                                currentValues.barometric,
                                currentValues.ambientTemp, 
                                currentValues.coolantTemp,
                                currentValues.intakeTemp,
                                currentValues.distance,
                                str(event)])

                # TODO: remove the timers since we're going to update by GPS
                cameraTimer += 1 # (cameraTimer + 1) % (camDelay / delay)
                #sleep(delay)

class Time():
    def __init__(self, Y, M, D, h, m, s, ms):
        self.year = Y
        self.month = M
        self.day = D
        self.hour = h
        self.minute = m
        self.second = s
        self.millisecond = ms

def formatDate(gpsDate):
    temp = str(gpsDate)
    temp = temp.split('-')
    
    year = temp[0]
    month = temp[1]
    time = temp[2]
    day = time.split('T')
    time = day[1].split(':')
    day = day[0]
    hour = time[0]
    minute = time[1]
    second = time[2].split('.')
    millisecond = second[1][:-1]
    second = second[0]

    time = Time(year, month, day, hour, minute, second, millisecond)
    return time

def setClock():
    # Waits until a time is received
    while str(gpsd.fix.time) == "nan":
        pass

    # Set the system time from GPS clock
    #print gpsd.fix.time
    time = formatDate(gpsd.fix.time)
    
    dateString = "date +%Y%m%d -s " + time.year + time.month + time.day
    timeString = "date +T -u -s " + time.hour + ":" + time.minute + ":" + time.second + "." + time.millisecond

    dateArgs = ['date', '+%Y%m%d', '-s', time.year + time.month + time.day]
    timeArgs = ['date', '+T', '-u', '-s', time.hour + ":" + time.minute + ":" + time.second + "." + time.millisecond]

    # FNULL used to hide output from subprocess calls
    FNULL = open(os.devnull, 'w')
    subprocess.call(dateArgs, stdout=FNULL)
    subprocess.call(timeArgs, stdout=FNULL)

def killThreads():
    print "Killing threads"
    if Testing:
        filetest.running = False
    else:
        bluetooth.running = False
    parse.running = False
    logger.running = False
    capture.running = False
    gps.running = False
    cameraQ.put("kill")
    valueQ.put("kill")

if __name__ == "__main__":
    print "Initializing..."
    if Testing:
        filetest = FileThread()
    else:
        bluetooth = BluetoothThread()

    camera = initCam()
    capture = CameraThread(camera)
    bluetooth = BluetoothThread()
    parse = ParseThread()
    logger = LoggerThread()
    gps = GPSThread()

    testLog = open('garbage.log', 'w')

    try:
        print "Starting GPS...\n"
        gps.start()

        print "Getting time from GPS...\n"
        setClock()

        print "Waiting for location lock"
        if not Inside:
            while str(gpsd.fix.latitude) == "nan" or str(gpsd.fix.latitude) == "0.0":
                pass

        print "Starting threads...\n"
        if Testing:
            filetest.start()
        else:
            bluetooth.start()

        parse.start()
        capture.start()
        logger.start()
        
        # Waiting for an interrupt
        while True:
            pass

    except (KeyboardInterrupt, SystemExit):
        killThreads()

    if Testing:
        filetest.join()
    else:
        bluetooth.join()
    parse.join()
    logger.join()
    capture.join()
    gps.join()

    camera.release()

    print "Exiting..."
