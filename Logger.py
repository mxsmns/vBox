#! /usr/bin/python
# Written by Clifton Rapier December 2014

import datetime
import os

def safeWrite(filename, value):
    filename.write(value)
    filename.flush()
    os.fsync(filename.fileno())

def get_timestamp():
        now = datetime.datetime.now()
        return "%04d" % now.year + "%02d" % now.month + "%02d" % now.day + "%02d" % now.hour + "%02d" % now.minute + "%02d" % now.second + "%03d" % (0.001 * now.microsecond)

# Logger Class
class FileLogger:
    def __init__(self, filename, quantity_unit_pairs):
        self.file = open(filename, 'w')
        self.quantities = []
        self.units = []
        self.num_labels = 3

        titleEntry = 'TimeStamp Lat Lon'
        for pair in quantity_unit_pairs:
            if len(pair) == 2:
                titleEntry = titleEntry + ' ' + pair[0] + '-' + pair[1]
                self.num_labels += 1
            elif len(pair) == 1 and pair[0] == 'Events':
                titleEntry = titleEntry + ' ' + pair[0]
                self.num_labels += 1
            else:
                print 'Quantity Unit Label: ' + str(pair) + ', should be a list of 2 strings.'
        titleEntry = titleEntry + '\n'
        safeWrite(self.file, titleEntry)

    def log(self, values):
        if len(values) == self.num_labels:
            entry = values[0]
            for data in values[1:]:
                entry = entry + ' ' + data
            entry = entry + '\n'
            safeWrite(self.file, entry)
        else:
            print 'Entry: ' + str(entry) + ', needs ' + str(self.num_labels) + ' entries.'
