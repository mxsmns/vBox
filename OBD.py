#! /usr/bin/python
# Written by Clifton Rapier December 2014
# Modified by Max Simmons December 2014

import struct

pids = {
        '0d01' : 'PID_SPEED',
        '2f01' : 'PID_FUEL_LEVEL',
        '0501' : 'PID_COOLANT_TEMP',
        '0401' : 'PID_ENGINE_LOAD',
        '0c01' : 'PID_RPM',
        '1101' : 'PID_THROTTLE',
        '1f01' : 'PID_RUNTIME',
        '3101' : 'PID_DISTANCE',
        '5901' : 'PID_ENGINE_FUEL_RATE',
        '5b01' : 'PID_ENGINE_TORQUE_PERCENTAGE',
        '3301' : 'PID_BAROMETRIC',
        '4601' : 'PID_AMBIENT_TEMP',
        '0f01' : 'PID_INTAKE_TEMP',
        'f00a' : 'PID_GPS_LATITUDE',
        'f00b' : 'PID_GPS_LONGITUDE',
        'f00c' : 'PID_GPS_ALTITUDE',
        'f00d' : 'PID_GPS_SPEED',
        'f00e' : 'PID_GPS_HEADING',
        'f00f' : 'PID_GPS_SAT_COUNT',
        'f010' : 'PID_GPS_TIME',
        'f020' : 'PID_ACC',
        'f021' : 'PID_GYRO'
        }


class OBDData():
    speed = 1
    fuel = 2
    coolantTemp = 3
    engineLoad = 4
    rpm = 5
    throttle = 6
    distance = 7
    barometric = 8
    ambientTemp = 9
    intakeTemp = 10

class PID:
    def __init__(self, value, name):
        self.raw = value
        self.name = name

def lookupPID(value):
    temp = ''.join(value.split()[4:6])
    #print temp
    # temp = ''.join(temp)

    if temp in pids:
        return pids[temp]
    return 'UNKNOWN'

def getChecksum(buffer):
    checksum = 0
    #print buffer
    buffer = buffer.split()
    # Only care about the first 12 bytes to calculate the checksum
    for i in range (0, 12):
        #print buffer[i]
        try:
            temp = int(buffer[i], 16)
            checksum = checksum ^ int(temp, 16)
        except:
            pass

    return checksum

class OBDValue:
    """Structure to hold OBD data after parsing"""
    def __init__(self, value, pid):
        self.pid = pid
        self.raw = ' '.join(value)
        # Probably don't really need to keep this here
        #self.checksum = int(value[7], 16)
        #temp = value[8:12]
        temp = value.split()[8:12]
        #temp = temp.split()
        temp = reversed(temp)
        test = ''.join(temp)
        #print temp
        # print test
        try:
            self.value = struct.unpack('f', struct.pack('i', int(test, 16)))[0]
        except:
            self.value = 0.0
        # print self.value
        #struct.unpack('!f', value.decode('hex'))[0]

class CurrentOBD:
    def __init__(self):
        self.speed = "XX"
        self.fuel = "XX"
        self.coolantTemp = "XX"
        self.engineLoad = "XX"
        self.rpm = "XX"
        self.throttle = "XX"
        self.distance = "XX"
        self.barometric = "XX"
        self.ambientTemp = "XX"
        self.intakeTemp = "XX"

    def update(self, quantity, value):
        if quantity == OBDData.speed:
            self.speed = value
        elif quantity == OBDData.fuel:
            self.fuel = value
        elif quantity == OBDData.coolantTemp:
            self.coolantTemp = value
        elif quantity == OBDData.engineLoad:
            self.engineLoad = value
        elif quantity == OBDData.rpm:
            self.rpm = value
        elif quantity == OBDData.throttle:
            self.throttle = value
        elif quantity == OBDData.distance:
            self.distance = value
        elif quantity == OBDData.barometric:
            self.barometric = value
        elif quantity == OBDData.ambientTemp:
            self.ambientTemp = value
        elif quantity == OBDData.intakeTemp:
            self.intakeTemp = value
