import cv2

def initCam():
	camera_port = 0
	camera = cv2.VideoCapture(camera_port)
	# Set the width of the capture
	camera.set(3,1280)
	# Set the height of the capture
	camera.set(4,720)

	return camera

def getImage(camera):
	retval, im = camera.read()
	return im

def takePicture(camera, filename):
	im = getImage(camera)
	# In tests, bmp had the fastest write times
	filename = filename + '.bmp'
	cv2.imwrite(filename, im)
